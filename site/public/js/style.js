$(document).ready(function(){

	var slider1 = new Slider('#numStaff', {
		min: 1,
		max: 100,
		step: 1,
		selection: 'none',
		ticks: [1,100],
		formatter: function(value) {
			return null;
		}
	})

	var slider2 = new Slider('#avgSalary', {
		min: 1000,
		max: 100000,
		orientation: 'horizontal',
		selection: 'none',
		step: 10,
		ticks: [1000,100000],
		formatter: function(value) {
			return null;
		}
	})

	var slider3 = new Slider('#grossMarginPercent', {
		min: 10,
		max: 90,
		step: 1,
		selection: 'none',
		ticks: [10,90],
		formatter: function(value) {
			return null;
		}
	})

	var slider4 = new Slider('#timeWasted', {
		min: 8,
		max: 40,
		step: 1,
		selection: 'none',
		ticks: [8,40],
		formatter: function(value) {
			return null;
		}
	})


	var sliders = {
		'num-staff': slider1,
		'avg-salary': slider2,
		'gross-margin-percent': slider3,
		'time-wasted': slider4
	}

	function sliderData() {
		// var numStaff = parseInt(document.getElementById('numStaff').value)
		var numStaff = slider1.getValue()
		var avgSalary = parseInt(document.getElementById('avgSalary').value)
		var grossMarginPercent = parseInt(document.getElementById('grossMarginPercent').value)
		var timeWasted = Math.round(160*(parseInt(document.getElementById('timeWasted').value)/100))

		var averageRate = Math.round((avgSalary/1600)*1.15*numStaff);

		monthlyCostOfTimeWaste = timeWasted*averageRate;
		monthlyOpportunityCost = timeWasted*averageRate/(1-(grossMarginPercent/100));
		totalMonthlyCostOfBadDocumentation = Math.round(monthlyOpportunityCost + monthlyCostOfTimeWaste);
		totalAnnualCostOfBadDocumentation = Math.round(totalMonthlyCostOfBadDocumentation*12);
		return {
			averageRate: averageRate,
			monthlyCostOfTimeWaste: monthlyCostOfTimeWaste,
			monthlyOpportunityCost: monthlyOpportunityCost,
			totalMonthlyCostOfBadDocumentation: totalMonthlyCostOfBadDocumentation,
			totalAnnualCostOfBadDocumentation: totalAnnualCostOfBadDocumentation
		}
	}

	$selected = $('#total-cost-time-toggle')

	function addCommas(str) {
		str += '';
		var x = str.split('.');
		var x1 = x[0];
		var x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
	return x1 + x2;
	}

	$('input[type=text]').on('keypress', function(e){
		if (e.which == 13) {
			$(this).focusout();
		}
	})
	$('input[type=text]').on('focusout', function() {
		a = $(this).val()
		sliders[$(this).attr('name')].setValue(parseInt($(this).val()),true);
		$(this).val(a)
		updateValues();
	})

	function updateValues(){
		var costs = sliderData()
		var monthOption = addCommas(Math.round(costs['totalMonthlyCostOfBadDocumentation']))
		var yearOption = addCommas(Math.round(costs['totalAnnualCostOfBadDocumentation']))

		$('input[name=num-staff]').val(($('#numStaff').val()))
		$('input[name=avg-salary]').val(($('#avgSalary').val()))
		$('input[name=gross-margin-percent]').val(($('#grossMarginPercent').val()))
		$('input[name=time-wasted]').val(($('#timeWasted').val()))

		$('#monthly-cost-time-waste').text('$' + addCommas(costs['monthlyCostOfTimeWaste']))
		$('#monthly-opp-cost').text('$' + addCommas(Math.round(costs['monthlyOpportunityCost'])))

		if ($selected.val() === 'month') {
			$('#total-cost').text('$' + monthOption)
		} else {
			$('#total-cost').text('$' + yearOption)
		}

	}

	$('.slider').on('slide click', function(){
		updateValues()
	})

	$('#total-cost-time-toggle').on('change', function() {
		var costs = sliderData();
		var monthOption = addCommas(Math.round(costs['totalMonthlyCostOfBadDocumentation']))
		var yearOption = addCommas(Math.round(costs['totalAnnualCostOfBadDocumentation']))

		if ($selected.val() === 'month') {
			$('#total-cost').text('$' + monthOption)
		} else {
			$('#total-cost').text('$' + yearOption)
		}
	})

})
